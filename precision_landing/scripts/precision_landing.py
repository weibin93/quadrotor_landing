import rospy
from geometry_msgs.msg import Twist, PoseStamped
from tf2_ros import TransformListener, Buffer
import sys

def local_pose_callback(data):
    global local_pose
    local_pose = data

if __name__ == '__main__':
    # 获取输入参数
    vehicle_type = sys.argv[1]
    vehicle_id = sys.argv[2]
    # 初始化节点
    rospy.init_node(vehicle_type+'_'+vehicle_id+'_precision_landing')
    tfBuffer = Buffer()
    tflistener = TransformListener(tfBuffer)
    cmd_vel_enu = Twist()   
    local_pose = PoseStamped()
    
    @todo: set up PID gains
    @todo: e.g., Kp = 1.0 ...

    # 订阅当前位置
    rospy.Subscriber(vehicle_type+'_'+vehicle_id+"/mavros/local_position/pose", PoseStamped, local_pose_callback,queue_size=1)
    cmd_vel_pub = rospy.Publisher('/xtdrone/'+vehicle_type+'_'+vehicle_id+'/cmd_vel_enu', Twist, queue_size=1)
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        try:
            # 使用tfBuffer对象的lookup_transform方法来查找从map坐标系到tag_加上vehicle_id的坐标系之间的变换信息
            tfstamped = tfBuffer.lookup_transform('map', 'tag_'+vehicle_id, rospy.Time(0))
        except:
            continue
        print('tf:',tfstamped.transform.translation.x)
        print(local_pose.pose.position.x)
        
        @todo: design PID controllers for landing
        @todo: e.g., cmd_vel_enu.linear.x = ...

        print(cmd_vel_enu)
        cmd_vel_pub.publish(cmd_vel_enu)
        rate.sleep()

