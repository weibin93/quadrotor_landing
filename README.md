# 基于 AprilTag 的无人机精准降落

## 配置环境

> 在此默认你已经配置好了 ROS Noetic，PX4 (1.13)，MAVROS的环境，并能够正常使用 Gazebo 进行无人机的仿真。

我们首先配置 XTDrone 的环境，其是一个基于 PX4 和 ROS 的一个开源仿真器。由于篇幅限制，下面只总结了配置代码，详细解释可参考[官方教程文档](https://www.yuque.com/xtdrone/manual_cn/basic_config_13)。

### 安装依赖项

```
sudo apt install ninja-build exiftool ninja-build protobuf-compiler libeigen3-dev genromfs xmlstarlet libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev python3-pip gawk
```

```
pip3 install pandas jinja2 pyserial cerberus pyulog==0.7.0 numpy toml pyquaternion empy pyyaml 
```

```
pip3 install packaging numpy empy toml pyyaml jinja2 pyargparse kconfiglib jsonschema future 
```

### 克隆 XTDrone 源代码

```
git clone https://gitee.com/robin_shaun/XTDrone.git
cd XTDrone
git checkout 1_13_2
git submodule update --init --recursive
```

### 重新安装 Gazebo-11

首先，移除已安装 Gazebo：

```
sudo apt-get remove gazebo* 
sudo apt-get remove libgazebo*
sudo apt-get remove ros-noetic-gazebo* 
```

然后重新安装：

```
sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
```

```
wget https://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
```

```
sudo apt-get update
```

```
sudo apt-get install gazebo11
```

```
sudo apt-get install libgazebo11-dev
```

```
sudo apt upgrade
```

用以下命令检查是否安装成功：

```
gazebo
```

安装 Gazebo 插件：

```
sudo apt-get install ros-noetic-moveit-msgs ros-noetic-object-recognition-msgs ros-noetic-octomap-msgs ros-noetic-camera-info-manager  ros-noetic-control-toolbox ros-noetic-polled-camera ros-noetic-controller-manager ros-noetic-transmission-interface ros-noetic-joint-limits-interface
```

创建一个工作空间：

> 注意：我们在这一文档中统一用 `catkin_ws` 指代工作空间，但你可以自己命名这些文件夹以保证各工作空间拥有不同的名字。

```
mkdir -p ~/catkin_ws/src
mkdir -p ~/catkin_ws/scripts
cd catkin_ws && catkin init # 使用catkin_make话，则为cd catkin_ws/src && catkin_init_workspace
catkin build # 使用catkin_make话，则为 cd .. && catkin_make 
```

克隆 `gazebo_ros_pkgs` 源代码：

```
cd ~/catkin_ws/src
git clone https://github.com/ros-simulation/gazebo_ros_pkgs.git
cd gazebo_ros_pkgs
git checkout noetic-devel
cd ..
cd ..
catkin build
```

复制 XTDrone 自定义插件：

```
cd ~/catkin_ws
cp -r ~/XTDrone/sitl_config/gazebo_ros_pkgs src/
catkin build #开发者测试使用catkin_make会出问题，因此建议使用catkin build
```

检查安装是否成功：

```
# In one terminal:
roscore 
```

```
# In another terminal:
source ~/catkin_ws/devel/setup.bash
rosrun gazebo_ros gazebo
```

下载 Gazebo 模型：

> 🔗 链接: https://pan.baidu.com/s/1yr2YSqcFU16htMgy9lS9ig?pwd=un42 提取码: un42 复制这段内容后打开百度网盘手机App，操作更方便哦

将 Gazebo 模型移至默认目录（假设将上一步下载的模型解压至`~/Downloads`目录下）：

```
cd ~/Downloads
cp -r models ~/.gazebo
```

### 编译 PX4 源码

```
cd PX4_Firmware
git checkout -b xtdrone/dev v1.13.2
git submodule update --init --recursive
make px4_sitl_default gazebo
```

> ⚠️ 注意：这里我们切换并使用 PX4 一个专门的 1.13 版本的分支 `xtdrone/dev`。

### 修改 `~/.bashrc`

打开 `~/.bashrc` 文件：

```
gedit ~/.bashrc
```

将以下文本复制进 `~/.bashrc` 文件：

```
source ~/catkin_ws/devel/setup.bash
source ~/PX4_Firmware/Tools/setup_gazebo.bash ~/PX4_Firmware/ ~/PX4_Firmware/build/px4_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:~/PX4_Firmware
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:~/PX4_Firmware/Tools/sitl_gazebo
```

### 检查环境配置

```
# In a new terminal:
roslaunch px4 mavros_posix_sitl.launch
```

```
# In another terminal: (if you see "connected: True", then it means that MAVROS is connected with PX4 SITL)
rostopic echo /mavros/state
```

### 修改并重新编译 PX4 源码

将 XTDrone 中的自定义功能更新至 PX4:

```
cd ~/XTDrone
# 修改启动脚本文件
cp sitl_config/init.d-posix/* ~/PX4_Firmware/ROMFS/px4fmu_common/init.d-posix/
# 添加launch文件
cp -r sitl_config/launch/* ~/PX4_Firmware/launch/
# 添加世界文件
cp sitl_config/worlds/* ~/PX4_Firmware/Tools/sitl_gazebo/worlds/
# 修改部分插件
cp sitl_config/gazebo_plugin/gimbal_controller/gazebo_gimbal_controller_plugin.cpp ~/PX4_Firmware/Tools/sitl_gazebo/src
cp sitl_config/gazebo_plugin/gimbal_controller/gazebo_gimbal_controller_plugin.hh ~/PX4_Firmware/Tools/sitl_gazebo/include
cp sitl_config/gazebo_plugin/wind_plugin/gazebo_ros_wind_plugin_xtdrone.cpp ~/PX4_Firmware/Tools/sitl_gazebo/src
cp sitl_config/gazebo_plugin/wind_plugin/gazebo_ros_wind_plugin_xtdrone.h ~/PX4_Firmware/Tools/sitl_gazebo/include
# 修改CMakeLists.txt
cp sitl_config/CMakeLists.txt ~/PX4_Firmware/Tools/sitl_gazebo
# 修改部分模型文件
cp -r sitl_config/models/* ~/PX4_Firmware/Tools/sitl_gazebo/models/ 
# 替换同名文件
cd ~/.gazebo/models/
rm -r stereo_camera/ 3d_lidar/ 3d_gpu_lidar/ hokuyo_lidar/
```

```
# 借用TF工具坐标转换
sudo cp ~/XTDrone/sitl_config/launch/px4/*  /opt/ros/noetic/share/mavros/launch/
```

重新编译 PX4:

```
cd ~/PX4_Firmware
make clean
make px4_sitl_default gazebo
```

## AprilTag 精准降落流程

AprilTag 是一个视觉基准系统，可用于机器人，增强现实和相机校准等。 根据 AprilTag 可以可靠地计算标签相对于相机的 3D 位置，方向和 ID 号。这里我们使用 AprilTag 的 ROS 库来实现位姿估计与 ID 号计算。

将提供的 `apriltag_ros` 功能包放入自己工作空间中，并编译：

```
sudo apt install ros-noetic-apriltag 
cd apriltag
cp -r ./ ~/catkin_ws/src/
cd ~/catkin_ws/
catkin build apriltag_ros #或catkin_make
```

### 配置 Gazebo 环境

仿真环境中需要用到 AprilTag 二维码模型，用于使无人机标记降落位置。该模型在 `./precision_landing/urdf` 目录下，我们需要将其添加到 Gazebo 模型路径中：
```
cd ./precision_landing/urdf
cp -r tag36h11_0/ ~/.gazebo/models/
```

### 启动仿真

首先启动带有下视相机的 iris 无人机和 AprilTag 模块的仿真文件，其位于 `./precision_landing/world` 目录下，我们需要将其添加到 PX4 安装目录下的 `world` 文件夹中：
```
cd ./precision_landing/world
cp outdoor2_precision_landing.world ~/PX4_Firmware/Tools/sitl_gazebo/worlds
```
然后将 `precision_landing.launch` 文件添加到 PX4 安装目录下的 `launch` 文件夹中，并执行：

```
cd ./precision_landing/launch
cp precision_landing.launch ~~/PX4_Firmware/launch
roslaunch px4 precision_landing.launch 
```

> 备注：启动后会发现不断报错，这是因为我们给 base_link 加了 namespace，所以 mavros 的base_link_frd 坐标系找不到 base_link。这个错误不影响使用，不用理会。
>
> ```
> [ERROR] [1617509823.832162747, 3.666000000]: ODOM: Ex: Could not find a connection between 'iris_0/base_link' and 'base_link_frd' because they are not part of the same tree.Tf has two or more unconnected trees.
> ```

### 启动无人机通信

打开一个新的终端，进入 Python 脚本文件夹（以下剩余步骤中的 Python 脚本都需要在该文件夹目录下执行）：

```
cd ./precision_landing/scripts
```

并运行：

```
python3 multirotor_communication.py iris 0
```

### 使用 Gazebo 真值

由于我们需要机体在 Gazebo 的地图坐标系下的位姿，因此定位需要使用 Gazebo 真值，此外使用 Gazebo 真值定位的精度也更高，便于事先精准降落。

> 注意：我们需要事先要修改 EKF2 设置，改为视觉定位模式，通过注释来修改不同的参数如下：
>
> ```
> gedit ~/PX4_Firmware/build/px4_sitl_default/etc/init.d-posix/rcS
> ```
>
> ```
> 	# GPS used
> 	#param set EKF2_AID_MASK 1
> 	# Vision used and GPS denied
> 	param set EKF2_AID_MASK 24
> 
> 	# Barometer used for hight measurement
> 	#param set EKF2_HGT_MODE 0
> 	# Barometer denied and vision used for hight measurement
> 	param set EKF2_HGT_MODE 3
> ```

打开一个新的终端输入下列命令，获取位姿真值：
```
python get_local_pose.py iris 1
```

### 使用键盘起飞

打开一个新的终端输入下列命令，启动键盘控制：

```
python multirotor_keyboard_control.py iris 1 vel
```

推荐起飞流程：按 `i` 把向上速度加到0.3以上，再按 `b` 切 offboard 模式，最后按 `t` 解锁起飞。到达一定高度后按 `s` 切换 hover 模式悬停，使无人机到 AprilTag 的上方。

> 通过键盘控制 iris 的解锁/上锁 (arm/disarm)，修改飞行模式，飞机速度等。使用 offboard 模式起飞，这时起飞速度要大于 0.3m/s 才能起飞（即：upward velocity 需要大于0.3）。注意，飞机要先解锁才能起飞！飞到一定高度后可以切换为 hover 模式悬停，再运行自己的飞行脚本，或利用键盘控制飞机。

### 启动 `apriltag_ros`

打开一个新的终端输入下列命令，启动 `apriltag_ros`：

```
source ~/catkin_ws/devel/setup.bash
roslaunch apriltag_ros precision_landing_detection.launch
```

### 启动精准降落代码

关闭键盘控制终端或在该终端中按 `ctrl+C` 停止程序运行，然后新建终端输入下列命令，启动精准降落脚本：

```
python3 precision_landing.py iris 0
```

## 任务

1. 熟悉 `apriltag_ros` 包及其他代码文件
2. 在 `precision_landing.py` 代码中，完成降落控制代码的实现（例如 PID 控制）
3. （可选）调整 `precision_landing.launch` 中无人机在 Gazebo 中的初始位置（现在为 `-0.1`），或尝试无人机在不同高度开始降落，验证降落表现
4. 撰写报告（推荐用 markdown 文档）
5. 录制降落视频（建议不超过 1 分钟）

作业递交：PDF格式报告，`precision_landing.py` 代码，视频
